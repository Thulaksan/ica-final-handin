﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThAmCo.Events.Services
{
    public class ReservationPostDto
    {
        public DateTime EventDate { get; set; }

        public string VenueCode { get; set; }

        public string StaffId { get; set; }
    }
}
