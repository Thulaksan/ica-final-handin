﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThAmCo.Events.Data;
using ThAmCo.Events.Services;
using Microsoft.EntityFrameworkCore;
using ThAmCo.Events.Models;

namespace ThAmCo.Events.Controllers
{

        public class AvailabilityController : Controller
        {
        private readonly EventsDbContext _context;

        public AvailabilityController(EventsDbContext context)
        {
            _context = context;
        }


        // GET: Availability
        public async Task<ActionResult> Index([FromQuery]int? EventId)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (EventId == null)
            {
                return BadRequest();
            }

            var EventsDb = _context.Events.FirstOrDefault(th => th.Id == EventId);
            String TypeId = EventsDb.TypeId;
            String Date = EventsDb.Date.ToLongDateString();
            var StartDate = Date;
            var EndDate = Date;

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:23652");
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromSeconds(5);

            IEnumerable<AvailabilityGetDto> avail = null;
            try
            {

            var response = await client.GetAsync("/api/Availability?eventType="+TypeId+"&beginDate="+Date+"&endDate="+Date);
            response.EnsureSuccessStatusCode();
            avail = await response.Content.ReadAsAsync<IEnumerable<AvailabilityGetDto>>();
            }
            catch (HttpRequestException e)
            {

                avail = Array.Empty<AvailabilityGetDto>();
            }
            
            return View(avail);
        }

        [HttpGet]
        public IActionResult Create(string id, DateTime date /* type */ )
        {

            ReservationCreateViewModel a = new ReservationCreateViewModel();
            a.VenueCode = id;
            a.EventDate = date;

            return View(a);
        }

         
        [HttpPost]
        public async Task<IActionResult> ACreate([FromForm] ReservationCreateViewModel a)
        {
            

            if (ModelState.IsValid)
            {

                var ReservationPostDto = new ReservationPostDto
                {
                    EventDate = a.EventDate,
                    VenueCode = a.VenueCode,
                    StaffId = a.StaffId
                };
                    var client = new HttpClient();
                    client.BaseAddress = new Uri("http://localhost:23652");
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    client.Timeout = TimeSpan.FromSeconds(5);

                    var post = await client.PostAsJsonAsync("api/Reservations", ReservationPostDto);
                    post.EnsureSuccessStatusCode();


                return RedirectToAction("Index", "Events");


                // Create http POST to API to reserve
                // In Venues API -> Resevation Controller -> CreateReservation([FromBody] ReservationPostDto reservation) returns string
                // Save returned string loacally


            }

            return RedirectToAction("Index", "Events");

        }

        }
}